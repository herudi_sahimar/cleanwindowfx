# Clean Window For Javafx#
Usage in main class :

```
#!java
public class TestClean extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        //Your FXML
        Parent root = FXMLLoader.load(getClass().getResource("/testclean/view/test.fxml"));
        
        //CleanWindowFX
        CleanWindowFX cw = new CleanWindowFX();
        cw.setTitle("TEST APP");
        cw.setScene(root);
        cw.setMaximized(true);
        cw.show(stage);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}

```

see : http://rudy-007.com/javafx-cleanwindowfx/