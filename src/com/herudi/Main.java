/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.herudi;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.stage.Stage;

/**
 *
 * @author Herudi
 */

public class Main extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        CleanWindowFX cs = new CleanWindowFX();
        Parent root = FXMLLoader.load(getClass().getResource("/com/herudi/view/test.fxml"));
        cs.setScene(root);
        cs.show(stage);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
