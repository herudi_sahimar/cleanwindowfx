/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.herudi.util;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author Herudi
 */
public abstract class ViewFX {

    private String viewName;

    public ViewFX() {
    }

    public Parent getView() {
        try {
            setViewName(getClass().getSimpleName());
            return FXMLLoader.load(getClass().getResource(getViewName().replace("Controller", ".fxml")));
        } catch (Exception e) {
            throw new RuntimeException("Node Not Found");
        }
    }

    public Parent getViewControl() {
        try {
            setViewName(this.getClass().getSimpleName());
            String pack = "/" + this.getClass().getPackage().getName().replace(".", "/").replace("controller", "view/");
            return FXMLLoader.load(getClass().getResource(pack + getViewName().replace("Controller", ".fxml")));
        } catch (Exception e) {
            throw new RuntimeException("Node Not Found");
        }
    }

    public Parent getViewControlWithFitToParent() {
        try {
            Parent view = getViewControl();
            AnchorPane.setTopAnchor(view, 0.0);
            AnchorPane.setRightAnchor(view, 0.0);
            AnchorPane.setLeftAnchor(view, 0.0);
            AnchorPane.setBottomAnchor(view, 0.0);
            return view;
        } catch (Exception e) {
            throw new RuntimeException("Node Not Found");
        }

    }

    public Parent getViewWithFitToParent() {
        try {
            Parent view = getView();
            AnchorPane.setTopAnchor(view, 0.0);
            AnchorPane.setRightAnchor(view, 0.0);
            AnchorPane.setLeftAnchor(view, 0.0);
            AnchorPane.setBottomAnchor(view, 0.0);
            return view;
        } catch (Exception e) {
            throw new RuntimeException("Node Not Found");
        }

    }

    public String getViewName() {
        return viewName;
    }

    public void setViewName(String viewName) {
        this.viewName = viewName;
    }

    public void ViewControlWithFitToParent(AnchorPane parent, Node node) {
        parent.getChildren().setAll(node);
    }

}

