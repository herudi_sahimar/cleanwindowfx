/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.herudi;

import com.herudi.controller.CleanWindowController;
import com.herudi.util.ResizeHelper;
import static com.herudi.controller.CleanWindowController.*;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author Herudi
 */
public class CleanWindowFX {

    boolean maxi;
    Node node;
    String title;

    public CleanWindowFX() {

    }

    public void show(Stage stage) {
        try {
            AnchorPane.setTopAnchor(node, 0.0);
            AnchorPane.setRightAnchor(node, 0.0);
            AnchorPane.setLeftAnchor(node, 0.0);
            AnchorPane.setBottomAnchor(node, 0.0);
            pane.getChildren().add(node);
            titleWIn = title;
            CleanWindowController clean = new CleanWindowController();
            Parent root = clean.getViewControl();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.initStyle(StageStyle.UNDECORATED);
            ResizeHelper.addResizeListener(stage);
            if (maxi) {
                Rectangle2D screen = Screen.getPrimary().getVisualBounds();
                stage.setX(screen.getMinX());
                stage.setY(screen.getMinY());
                stage.setWidth(screen.getWidth());
                stage.setHeight(screen.getHeight());
            }
            stage.show();
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }

    }

    public void setMaximized(boolean maximized) {
        maxi = maximized;
    }

    public void setScene(Node node) {
        this.node = node;
    }
    
    public void setTitle(String title){
        this.title = title;
    }

}
