/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.herudi.controller;

import com.herudi.util.ViewFX;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Screen;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Herudi
 */
public class CleanWindowController extends ViewFX implements Initializable {

    @FXML
    private AnchorPane paneBorder;
    @FXML
    private Label title;
    @FXML
    private Button fullscreen;
    @FXML
    private Button minimize;
    @FXML
    private Button maximize;
    @FXML
    private Button close;
    @FXML
    private AnchorPane paneLoad;
    @FXML
    private Button resize;
    Stage stage;
    double x = 0, y = 0;
    final Delta dragDelta = new Delta();
    @FXML
    private AnchorPane paneDrag;
    public static AnchorPane pane = new AnchorPane();
    public static String titleWIn = "";
    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        AnchorPane.setTopAnchor(pane, 0.0);
        AnchorPane.setRightAnchor(pane, 0.0);
        AnchorPane.setLeftAnchor(pane, 0.0);
        AnchorPane.setBottomAnchor(pane, 0.0);
        paneLoad.getChildren().setAll(pane);
        title.setText(titleWIn);
        paneDrag.setOnMousePressed((MouseEvent event) -> {
            stage = (Stage) paneDrag.getScene().getWindow();
            dragDelta.x = stage.getX() - event.getScreenX();
            dragDelta.y = stage.getY() - event.getScreenY();
        });
        paneDrag.setOnMouseDragged((MouseEvent event) -> {
            stage = (Stage) paneDrag.getScene().getWindow();
            stage.setX(event.getScreenX() + dragDelta.x);
            stage.setY(event.getScreenY() + dragDelta.y);
        });

        paneDrag.setOnMouseClicked((MouseEvent event) -> {
            if (event.getClickCount() == 2) {
                stage = (Stage) paneDrag.getScene().getWindow();
                Rectangle2D screen = Screen.getPrimary().getVisualBounds();
                if (stage.getWidth() == screen.getWidth()) {
                    stage.setWidth(1000);
                    stage.setHeight(600);
                    stage.centerOnScreen();
                    maximize.getStyleClass().setAll("decoration-button-maximize");
                } else {
                    stage.setX(screen.getMinX());
                    stage.setY(screen.getMinY());
                    stage.setWidth(screen.getWidth());
                    stage.setHeight(screen.getHeight());
                    maximize.getStyleClass().setAll("decoration-button-restore");
                }
            }
        });
    }

    class Delta {
        double x, y;
    }

    @FXML
    private void aksifullscreen(ActionEvent event) {
        stage = (Stage) fullscreen.getScene().getWindow();
        if (!stage.isFullScreen()) {
            stage.setFullScreen(true);
        }else{
            stage.setFullScreen(false);
        }
    }

    @FXML
    private void aksiminimize(ActionEvent event) {
        stage = (Stage) minimize.getScene().getWindow();
        stage.setIconified(true);
    }

    @FXML
    private void aksiMaximized(ActionEvent event) {
        stage = (Stage) maximize.getScene().getWindow();
        Rectangle2D screen = Screen.getPrimary().getVisualBounds();
        if (stage.getWidth() == screen.getWidth()) {
            stage.setWidth(1000);
            stage.setHeight(600);
            stage.centerOnScreen();
            maximize.getStyleClass().setAll("decoration-button-maximize");
        } else {
            stage.setX(screen.getMinX());
            stage.setY(screen.getMinY());
            stage.setWidth(screen.getWidth());
            stage.setHeight(screen.getHeight());
            maximize.getStyleClass().setAll("decoration-button-restore");
        }
    }

    @FXML
    private void aksiClose(ActionEvent event) {
        Platform.exit();
        System.exit(0);
    }

    @FXML
    private void aksiResize(ActionEvent event) {
    }

}
